package com.ukrim.ppbl.aplikasimakanan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.iammert.library.readablebottombar.BottomBarItemConfig
import com.ukrim.ppbl.aplikasimakanan.model.MealDetail
import com.ukrim.ppbl.aplikasimakanan.model.MealDetailResponse
import com.ukrim.ppbl.aplikasimakanan.model.MealResponse
import com.ukrim.ppbl.aplikasimakanan.presenter.MealPresenter
import com.ukrim.ppbl.aplikasimakanan.ui.DessertFragment
import com.ukrim.ppbl.aplikasimakanan.ui.SeafoodFragment
import com.ukrim.ppbl.aplikasimakanan.view.CommonView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, fragment, fragment.javaClass.simpleName)
            .commit()
    }

    private val onBottomNavItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menu_seafood -> {
                    val fragment = SeafoodFragment.newInstance()
                    addFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.menu_dessert -> {
                    val fragment = DessertFragment.newInstance()
                    addFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val mHandler = Handler()
        val mPendingRunnable = Runnable {
        bottom_navigation.setOnNavigationItemSelectedListener(
            onBottomNavItemSelectedListener
        )

        val defaultFragment = SeafoodFragment.newInstance()
        addFragment(defaultFragment)
        }
        if(mPendingRunnable != null){
            mHandler.post(mPendingRunnable)
        }
    }

}
