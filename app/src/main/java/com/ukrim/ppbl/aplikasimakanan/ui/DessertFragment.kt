package com.ukrim.ppbl.aplikasimakanan.ui


import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.TypedValue
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ukrim.ppbl.aplikasimakanan.R
import com.ukrim.ppbl.aplikasimakanan.adapter.MakananAdapter
import com.ukrim.ppbl.aplikasimakanan.model.Meal
import com.ukrim.ppbl.aplikasimakanan.model.MealResponse
import com.ukrim.ppbl.aplikasimakanan.presenter.MealPresenter
import com.ukrim.ppbl.aplikasimakanan.ui.detail.DetailMakananActivity
import com.ukrim.ppbl.aplikasimakanan.util.Constant
import com.ukrim.ppbl.aplikasimakanan.view.CommonView
import kotlinx.android.synthetic.main.fragment_meals.*

class DessertFragment : Fragment(), CommonView, MakananAdapter.Listener {
    lateinit var presenter: MealPresenter
    lateinit var adapter: MakananAdapter
    var meals: MutableList<Meal> = mutableListOf()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_meals, container, false)
    }

    companion object {
        fun newInstance(): DessertFragment = DessertFragment()
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setDataMakanan()

    }

    private fun dpToPx(dp: Int): Int {
        val r = resources
        return Math.round(
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics
            )
        )
    }

    private fun setDataMakanan() {
        list_makanan.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(activity!!.applicationContext, 2)
        list_makanan.layoutManager = layoutManager as RecyclerView.LayoutManager?
        list_makanan.addItemDecoration(GridSpacingItemDecoration(2, dpToPx(8), true))
        list_makanan.itemAnimator = DefaultItemAnimator()
        adapter = MakananAdapter(meals, this)
        list_makanan.adapter = adapter
        presenter = MealPresenter(this, activity!!.applicationContext)
        presenter.getAllMealsByCategory("Dessert")
    }

    override fun showLoading() {
        progress_bar.visibility = View.VISIBLE
    }

    override fun error(error: Throwable) {
    }

    override fun success(anyResponse: Any) {
        val dataResponse = anyResponse as MealResponse
        meals.clear()
        meals.addAll(dataResponse.meals)
        adapter.notifyDataSetChanged()
    }

    override fun hideLoading() {
        progress_bar.visibility = View.GONE
    }

    override fun onItemClick(meal: Meal) {
        val intent = Intent(activity?.applicationContext,
            DetailMakananActivity::class.java)
        intent.putExtra(Constant.KEY_IDMEAL,meal.idMeal)
        startActivity(intent)
    }

    inner class GridSpacingItemDecoration(
        private val spanCount: Int,
        private val spacing: Int,
        private val includeEdge: Boolean
    ) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right =
                    spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }
}
