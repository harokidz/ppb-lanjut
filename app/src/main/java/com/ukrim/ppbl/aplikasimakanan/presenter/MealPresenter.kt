package com.ukrim.ppbl.aplikasimakanan.presenter

import android.content.Context
import com.ukrim.ppbl.aplikasimakanan.networking.RetrofitFactory
import com.ukrim.ppbl.aplikasimakanan.view.CommonView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MealPresenter(
    private val view: CommonView,
    private val context: Context
) {
    fun getAllMealsByCategory(category: String) {
        view.showLoading()
        val api = RetrofitFactory.create()
        val request = api.getAllMealsByCategory(category)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }
    fun getMealById(meadId: String) {
        view.showLoading()
        val api = RetrofitFactory.create()
        val request = api.getAllMealById(meadId)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response.mealsDetail[0])
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }
}