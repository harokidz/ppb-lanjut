package com.ukrim.ppbl.aplikasimakanan.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.ukrim.ppbl.aplikasimakanan.R
import com.ukrim.ppbl.aplikasimakanan.model.Meal
import com.ukrim.ppbl.aplikasimakanan.model.MealDetail
import com.ukrim.ppbl.aplikasimakanan.presenter.MealPresenter
import com.ukrim.ppbl.aplikasimakanan.util.Constant
import com.ukrim.ppbl.aplikasimakanan.view.CommonView
import kotlinx.android.synthetic.main.activity_detail_makanan.*

class DetailMakananActivity : AppCompatActivity(),CommonView {
    private lateinit var mealId: String
    private lateinit var presenter: MealPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_makanan)
        mealId = intent.getStringExtra(Constant.KEY_IDMEAL)
        presenter = MealPresenter(this,this)
        presenter.getMealById(mealId)
    }
    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun error(error: Throwable) {

    }

    override fun success(anyResponse: Any) {
        val mealDetail = anyResponse as MealDetail
        tv_nama_makanan.text = mealDetail.strMeal
        tv_kategori_makanan.text = mealDetail.strCategory
        tv_instruksi_makanan.text = mealDetail.strInstructions
        Glide.with(this)
            .load(mealDetail.strMealThumb)
            .into(image_makanan)
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }
}
