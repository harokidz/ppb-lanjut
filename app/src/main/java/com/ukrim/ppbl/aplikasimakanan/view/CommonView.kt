package com.ukrim.ppbl.aplikasimakanan.view

interface CommonView {
    fun showLoading()
    fun error(error : Throwable)
    fun success(anyResponse : Any)
    fun hideLoading()
}