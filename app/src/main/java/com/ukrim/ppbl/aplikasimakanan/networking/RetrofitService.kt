package com.ukrim.ppbl.aplikasimakanan.networking

import com.ukrim.ppbl.aplikasimakanan.model.MealDetailResponse
import com.ukrim.ppbl.aplikasimakanan.model.MealResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {
    @GET("filter.php")
    fun getAllMealsByCategory(@Query("c")type:String)
            : Deferred<MealResponse>

    @GET("lookup.php")
    fun getAllMealById(@Query("i")type:String)
            : Deferred<MealDetailResponse>

}